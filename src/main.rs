use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

static CELL_MARKER: &str = "[CELL] ";
static VAR_CELL_MARKER: &str = "[VAR_CELL] ";

static CIRC_QUEUE: &str = "CIRC_QUEUE: ";
static EDGE_OUTBUF: &str = "EDGE_OUTBUF: ";
static OR_CONN_OUTBUF: &str = "OR_CONN_OUTBUF: ";

#[derive(Debug, Default)]
struct CircQueueLine {
    nickname: String,
    circ_gid: u64,
    p_circ_id: u64,
    n_circ_id: u64,
    prev_queue_size: usize,
    next_queue_size: usize,
    chan_gid: u64,
    outbuf_size: usize,
    next_chan_identity: String,
}

#[derive(Debug, Default)]
struct EdgeOutbufLine{
    nickname: String,
    circ_gid: u64,
    p_circ_id: u64,
    n_circ_id: u64,
    prev_queue_size: usize,
    next_queue_size: usize,
    conn_gid: u64,
    prev_outbuf_size: usize,
    next_outbuf_size: usize,
}

#[derive(Debug, Default)]
struct OrConnBufLine {
    nickname: String,
    chan_gid: u64,
    conn_gid: u64,
    prev_outbuf_size: usize,
    next_outbuf_size: usize,
    next_chan_identity: String,
}

#[derive(Debug)]
enum Cell {
    CircQueue(CircQueueLine),
    EdgeOutbuf(EdgeOutbufLine),
    OrConn(OrConnBufLine),
}

#[derive(Debug)]
enum VarCell {
    OrConn(OrConnBufLine),
}

#[derive(Debug)]
enum Line {
    Cell(Cell),
    VarCell(VarCell),
}

impl CircQueueLine {
    fn parse(line: &String) -> Option<CircQueueLine> {
        if let Some(idx) = line.find(CIRC_QUEUE) {
            let mut cql: CircQueueLine = Default::default();
            let tokens = line
                .get((idx + CIRC_QUEUE.len())..)?
                .to_string()
                .split(' ')
                .map(|kv| kv.split("="))
                .map(|mut kv| (kv.next().unwrap().into(), kv.next().unwrap().into()))
                .collect::<HashMap<String, String>>();
            for (k, v) in tokens {
                match k.as_str() {
                    "nickname" => cql.nickname = v,
                    "circ_gid" => cql.circ_gid = v.parse().unwrap_or(0),
                    "p_circ_id" => cql.p_circ_id = v.parse().unwrap_or(0),
                    "n_circ_id" => cql.n_circ_id = v.parse().unwrap_or(0),
                    "p_queue" => cql.prev_queue_size = v.parse().unwrap_or(0),
                    "n_queue" => cql.next_queue_size = v.parse().unwrap_or(0),
                    "chan_gid" => cql.chan_gid = v.parse().unwrap_or(0),
                    "outbuf" => cql.outbuf_size = v.parse().unwrap_or(0),
                    "next_chan_id" => cql.next_chan_identity = v,
                    _ => (),
                }
            }
            return Some(cql);
        }
        None
    }
}

impl EdgeOutbufLine {
    fn parse(line: &String) -> Option<EdgeOutbufLine> {
        if let Some(idx) = line.find(EDGE_OUTBUF) {
            let mut cql: EdgeOutbufLine = Default::default();
            let tokens = line
                .get((idx + EDGE_OUTBUF.len())..)?
                .to_string()
                .split(' ')
                .map(|kv| kv.split("="))
                .map(|mut kv| (kv.next().unwrap().into(), kv.next().unwrap().into()))
                .collect::<HashMap<String, String>>();
            for (k, v) in tokens {
                match k.as_str() {
                    "nickname" => cql.nickname = v,
                    "circ_gid" => cql.circ_gid = v.parse().unwrap_or(0),
                    "p_circ_id" => cql.p_circ_id = v.parse().unwrap_or(0),
                    "n_circ_id" => cql.n_circ_id = v.parse().unwrap_or(0),
                    "p_queue" => cql.prev_queue_size = v.parse().unwrap_or(0),
                    "n_queue" => cql.next_queue_size = v.parse().unwrap_or(0),
                    "conn_gid" => cql.conn_gid = v.parse().unwrap_or(0),
                    "prev_outbuf" => cql.prev_outbuf_size = v.parse().unwrap_or(0),
                    "next_outbuf" => cql.next_outbuf_size = v.parse().unwrap_or(0),
                    _ => (),
                }
            }
            return Some(cql);
        }
        None
    }
}

impl OrConnBufLine {
    fn parse(line: &String) -> Option<OrConnBufLine> {
        if let Some(idx) = line.find(OR_CONN_OUTBUF) {
            let mut ocbl: OrConnBufLine = Default::default();
            let tokens = line
                .get((idx + OR_CONN_OUTBUF.len())..)?
                .to_string()
                .split(' ')
                .map(|kv| kv.split("="))
                .map(|mut kv| (kv.next().unwrap().into(), kv.next().unwrap().into()))
                .collect::<HashMap<String, String>>();
            for (k, v) in tokens {
                match k.as_str() {
                    "nickname" => ocbl.nickname = v,
                    "chan_gid" => ocbl.chan_gid = v.parse().unwrap_or(0),
                    "conn_gid" => ocbl.conn_gid = v.parse().unwrap_or(0),
                    "prev_outbuf" => ocbl.prev_outbuf_size = v.parse().unwrap_or(0),
                    "next_outbuf" => ocbl.next_outbuf_size = v.parse().unwrap_or(0),
                    "next_chan_id" => ocbl.next_chan_identity = v,
                    _ => (),
                }
            }
            return Some(ocbl);
        }
        None
    }
}

impl Cell {
    fn parse(line: &String) -> Option<Cell> {
        if let Some(idx) = line.find(CELL_MARKER) {
            let data = line.get((idx + CELL_MARKER.len())..)?.to_string();
            match CircQueueLine::parse(&data) {
                None => match OrConnBufLine::parse(&data) {
                    Some(c) => return Some(Cell::OrConn(c)),
                    None => match EdgeOutbufLine::parse(&data) {
                        Some(c) => return Some(Cell::EdgeOutbuf(c)),
                        None => (),
                    },
                },
                Some(c) => return Some(Cell::CircQueue(c)),
            }
        }
        None
    }
}

impl VarCell {
    fn parse(line: &String) -> Option<VarCell> {
        if let Some(idx) = line.find(VAR_CELL_MARKER) {
            let data = line.get((idx + VAR_CELL_MARKER.len())..)?.to_string();
            match OrConnBufLine::parse(&data) {
                Some(c) => return Some(VarCell::OrConn(c)),
                None => (),
            }
        }
        None
    }
}

impl Line {
    fn parse(line: &String) -> Option<Line> {
        if let Some(r) = Cell::parse(line) {
            return Some(Line::Cell(r));
        } else if let Some(r) = VarCell::parse(line) {
            return Some(Line::VarCell(r));
        }
        None
    }
}

fn main() {
    if let Ok(lines) = read_lines(std::env::args().nth(1).expect("No filname given")) {
        for l in lines {
            if l.is_err() {
                continue;
            }
            let line = l.unwrap();

            let _lm = Line::parse(&line);
        }
    }
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
